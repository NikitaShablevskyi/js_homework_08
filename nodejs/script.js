const fs = require('fs');
const path = require('path');
const nodemailer = require('nodemailer');

const excel = require('excel4node');
let workbook = new excel.Workbook();
let worksheet = workbook.addWorksheet('Sheet 1', {});
let style = workbook.createStyle({
    font: {
        color: '#000000',
        size: 12,
    },
});

const EMAIL_REGEX = /--EMAIL-TO=(.*)/i;
const DIR_REGEX = /--DIR=(.*)/i;
const TYPE_REGEX = /--TYPE=(.*)/i;
const PATTERN_REGEX = /--PATTERN=(.*)/i;
const MIN_SIZE_REGEX = /--MIN-SIZE=(.*)/i;
const MAX_SIZE_REGEX = /--MAX-SIZE=(.*)/i;
const SIZE_REGEX = /(\d+)(\D)/;

const extractParameterValue = regexp => {
    const [node, executable, ...params] = process.argv;
    let res = params.map(i => regexp.exec(i)).find(i => i);
    if (res) {
        return res[1];
    }
};

const extractFileSize = size => {
    if (size) {
        let sizeInBytes = Number(size.replace(SIZE_REGEX, '$1')) *
          transformMetricPrefix(size.replace(SIZE_REGEX, '$2'));
        if (sizeInBytes) {
            return sizeInBytes;
        }
    }
};

const transformMetricPrefix = (prefix) => {
    switch (prefix) {
        case 'G':
            return 1024 ** 3;
        case 'M':
            return 1024 ** 2;
        case 'K':
            return 1024;
        case 'B':
            return 1;
    }
    return undefined;
};

const parametersComplianceCheck = (file, stat, type, pattern, minSize, maxSize) => {
    if (pattern) {
        if (!new RegExp(pattern).exec(path.basename(file))) {
            return false;
        }
    }
    //If file or directory have some problems with security,or it`s broken
    if (!stat) {
        return false;
    }
    if (type) {
        switch (type) {
            case 'D':
                if (!stat.isDirectory()) {
                    return false;
                }
                break;
            case 'F':
                if (!stat.isFile()) {
                    return false;
                }
                if (minSize) {
                    if (stat.size < minSize) {
                        return false;
                    }
                }
                if (maxSize) {
                    if (stat.size > maxSize) {
                        return false;
                    }
                }
                break;
        }
    }
    return true;
};

const directoryLookUp = (dir, type, pattern, minSize, maxSize, callBack) => {
    let result = [];
    fs.readdir(dir, (error, list) => {
        if (error) return callBack(error);
        let pending = list.length;
        if (!pending) return callBack(null, result);
        list.forEach(file => {
            file = path.resolve(dir, file);
            fs.stat(file, (error, stat) => {
                if (parametersComplianceCheck(file, stat, type, pattern, minSize, maxSize)) {
                    result.push(file);
                }
                if (stat && stat.isDirectory()) {
                    directoryLookUp(file, type, pattern, minSize, maxSize, (error, branchResult) => {
                        result = result.concat(branchResult);
                        if (!--pending) callBack(null, result);
                    });
                } else {
                    if (!--pending) callBack(null, result);
                }
            });
        });
    });
};

const writeDataToExcel = (data) => {
    let currentRowIndex = 0;
    data.forEach(path => {
        currentRowIndex++;
        worksheet.cell(currentRowIndex, 1).string(`${path}`).style(style);
    });

    workbook.write('Excel.xlsx', (error) => {
        if (error) {
            console.log('Error occurred');
            console.log(error.message);
        }
    });
};

const sendMessage = () => {
    let properties = JSON.parse(fs.readFileSync('./smtp.properties','utf8'));

    let transporter = nodemailer.createTransport(properties);

    let message = {
        from: 'someEmail@gmail.com',
        to: `${extractParameterValue(EMAIL_REGEX)}`,
        subject: 'Excel with paths',
        text: 'Take your excel with paths',
        attachments: [
            {
                filename: 'Excel.xlsx',
                path: './Excel.xlsx'
            }
        ]
    };

     transporter.sendMail(message, (error, info) => {
        if (error) {
            console.log('Error occurred');
            console.log(error.message);
            return process.exit(1);
        }
        console.log('Message sent successfully!');
        console.log(info);
    });
};

const sendExcelToEmail = () => {
    directoryLookUp(extractParameterValue(DIR_REGEX), extractParameterValue(TYPE_REGEX),
      extractParameterValue(PATTERN_REGEX), extractFileSize(extractParameterValue(MIN_SIZE_REGEX)),
      extractFileSize(extractParameterValue(MAX_SIZE_REGEX)), function (error, result) {
          if (error) {
              console.log('Error occurred');
              console.log(error.message);
          }
          writeDataToExcel(result);
          sendMessage();
      });

};

sendExcelToEmail();
